
(function(app) {
  app(window.jQuery, window, document);
  }(function($, window, document) {
    $(function() {
      let url = 'https://jsonplaceholder.typicode.com/posts/';
      let cardArray = [];
      let data;
      let element = document.getElementById('cards-holder');

      function readFromUrl(url, id){
        fetch(url)
        .then(res => res.json())
        .then((out) => {
            createCards(out, id);
        })
        .catch(err => { throw err });
      }

      function main(){
        let iconTrash = document.createElement("i");
        iconTrash.className="fas fa-trash";
        document.getElementById('remover').prepend(iconTrash);
        let iconClose = document.createElement("i");
        iconClose.className="fas fa-bars";
        iconClose.id="menu";
        $('body').prepend(iconClose);
      } 

      function addCard(id){
        let arr = getCards();
        (arr.indexOf(id) > -1) ? console.log("complete") :readFromUrl(url, id);
      }

      function createSkuArray(){
          $('#card-trigger li a').map(function(){ 
            cardArray.push(parseInt(($(this).attr('data-sku'))));
        });
        return cardArray; 
      }

      function getCards(){
        let arr = [];
        $('#cards-holder div').map(function(index,obj){ 
          let str = obj.getAttribute('class');
          let id = str.substring(13);
          let val = parseInt(($(this).attr('data-sku')));
          arr.push(parseInt(id));
        });
        return arr; 
      }

      function createCards(data, id){
        let skuList;
        (!id) ? skuList = createSkuArray() :  skuList =[id];
          let l = cardArray.length;
          for(var i = 0; i < data.length-1; i++){
            if (skuList.includes(data[i].id)){
              let card = document.createElement("div");
              let id = data[i].id;
              let userId = data[i].userId;
              let title = data[i].title;
              let body = data[i].body;
              card.className="card card-id-"+id;
              let trash = document.createElement('i');
              trash.className = 'fa fa-times cancel';  
              let cardTitle = document.createElement("h4");
              let cardBody = document.createElement("p");
              cardTitle.className="card-title";
              cardTitle.innerHTML = title;
              cardBody.innerHTML = body;
              cardBody.className="card-body";
              card.appendChild(trash);
              card.appendChild(cardTitle);
              card.appendChild(cardBody);
              element.appendChild(card);
            }
          }  
          let len = document.querySelectorAll("#cards-holder div").length;
          adjustLayout();
      }

      $('#remover').on('click', function(){
        $('.card').remove();
      });

      $('body').on('click','#menu',function(){
        $('#menu').toggleClass('fa-bars fa-times');
        if ($('#card-trigger').css('visibility') == 'hidden'){
          $('#card-trigger').css('visibility', 'visible');
        } else{
         $('#card-trigger').css('visibility', 'hidden'); 
        }
      });

      $( '#cards-holder' ).on( 'click', 'i', function(){
        $(this).parent().remove();
        let len = document.querySelectorAll("#cards-holder div").length;
        adjustLayout();
      });

      $('#card-trigger li').on('click', function(){
        let str = $(this).find('a').html();
        let id = parseInt(str.substring(5));
        addCard(id);
      });

      function reportWindowSize() {

        let len = document.querySelectorAll("#cards-holder div").length;
        adjustLayout();
        if (window.innerWidth < 768){
          $('#menu').css('display', 'block');
          $('#menu').toggleClass('fa-bars fa-times');
          $('#menu').removeClass('fa-bars fa-times');
          $('#menu').addClass('fa-bars');
          $('#card-trigger').css('visibility', 'hidden');
        }else{
          $('#card-trigger').css('visibility', 'visible');
          $('#menu').css('display', 'none');
        }
      }
      function adjustLayout() {
        let len = document.querySelectorAll("#cards-holder div").length;
        if (window.innerWidth < 500){
          $('#cards-holder').css('grid-template-columns', '1fr');  
        }else{
          $('#cards-holder').css('grid-template-columns', 'repeat('+len+',1fr)');  
        }
      }

      main();
      window.onresize = reportWindowSize;
    });
  }
));
